package com.services.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "event")
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer Id;

    @Column(name = "title")
    private String Title;

    @Column(name = "location")
    private String Location;

    @Column(name = "date")
    private Date Date;

    @Column(name = "participant")
    private String Participant;

    @Column(name = "note")
    private String Note;

    public Event() {
    }

    public Event(String title, String location, java.util.Date date, String participant, String note) {
        Title = title;
        Location = location;
        Date = date;
        Participant = participant;
        Note = note;
    }

    public Event(Integer id, String title, String location, java.util.Date date, String participant, String note) {
        Id = id;
        Title = title;
        Location = location;
        Date = date;
        Participant = participant;
        Note = note;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public Date getDate() {
        return Date;
    }

    public void setDate(Date date) {
        Date = date;
    }

    public String getParticipant() {
        return Participant;
    }

    public void setParticipant(String participant) {
        Participant = participant;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String note) {
        Note = note;
    }

}
